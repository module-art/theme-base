<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="Module-art, Sylvestre Lambey">
    {{--<link rel="icon" type="image/png" href="/images/logo_example_128.png" sizes="16x16" />--}}
    <link rel="icon" href="/favicon.ico" />

    @yield('title')

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/css/fontawesome/all.min.css">
    <link rel="stylesheet" href="/css/themes/themebase/styles.css">

  </head>
  <body class="">

    @yield('sidebar')

    <div class="container-fluid">

      <section id="global-wrapper">

        <div class="head">
          @yield('menu')
        </div>

        @yield('contenu')

      </section>

      <a class="cd-top">Haut de page</a>

      @yield('footer')
    
    </div>

    <script>
  //here is define js current_theme global var with php
  var current_theme = '{{ config('modules.theme') }}';
    </script>
    <script src="/js/app.js"></script>
    <script src="/js/themes/themebase/scripts.js"></script>

    @yield('scripts')

  </body>
</html>
