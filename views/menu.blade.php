<div class="d-md-none">
    <a class="navbar-brand ml-0" href="{{ route('page.home') }}" id="sm-logo"><img src="/storage/themebase/files/icons/logo_module_light.svg" alt="logo"></a>
</div>
<nav class="navbar navbar-expand-md">
  @if(!isset($operation)){{-- case for admin pages --}}
    @if(Auth::check() && Auth::user()->role == 'admin')

      <button type="button" class="btn btn-info sidebarCollapse">
        <i class="fas fa-align-left"></i>
        <span>{{ Auth::user()->name }}</span>
      </button>

    @endif
  @endif
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars01" aria-controls="navbars03" aria-expanded="false" aria-label="Toggle navigation">
    <!-- <span class="navbar-toggler-icon"></span> -->
    <i class="fas fa-bars"></i>
  </button>

  <div class="collapse navbar-collapse mr-auto" id="navbars01">
    <a class="navbar-brand ml-0 d-none d-md-block" href="{{ route( (Auth::check() && Auth::user()->role == 'admin')? 'back_page.show' : 'page.show', 'accueil') }}" id="logo"><img src="/storage/themebase/files/icons/logo_module_light.svg" alt="logo"></a>
    <ul class="navbar-nav justify-content-end">
      @if($menus->count() > 0)
        @foreach($menus as $menu)
          @if(isset($page) && $page->slug == $menu->slug)
            <li id="current_page" class="nav-item active">
          @else
            <li class="nav-item">
          @endif
          @if(Auth::check())
            {{ link_to_route('back_page.show', $menu->menu_title, $menu->slug, ['class' => 'nav-link']) }}
          @else
            {{ link_to_route('page.show', $menu->menu_title, $menu->slug, ['class' => 'nav-link']) }}
          @endif
        </li>
        @endforeach
      @endif
    </ul>

  </div>
</nav>
