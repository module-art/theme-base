
{{--<button class="btn btn-sm btn-outline-danger btn-destroy" ><i class="fas fa-trash-alt"></i></button>
<div class="heading editrubrique{{ isset($y) && $y == 0 ? ' first' : '' }}" 
@if(isset($rubrique->background_img_url))
  style="background-image: url('{!! asset( $rubrique->background_img_url ) !!}');
  background-image: -webkit-image-set( url('{!! asset( $rubrique->background_img_url ) !!}') 1x, url('{!! asset( $rubrique->background_hd_url ) !!}') 2x );
    background-image: image-set( url('{!! asset( $rubrique->background_img_url ) !!}') 1x, url('{!! asset( $rubrique->background_hd_url ) !!}') 2x );" 
@endif
data-rubrique_id="{!! $rubrique->id !!}">
  {!! $rubrique->contenu !!}
</div>--}}

<button class="btn btn-sm btn-outline-secondary btn-publish" title="{!! $rubrique->publie ? 'Masquer' : 'Publier' !!} cette rubrique">
  {!! $rubrique->publie ? '<span class="published"><i class="fas fa-eye-slash"></i></span>' : '<span class="unpublished"><i class="fas fa-eye"></i></span>' !!}
</button>
<button class="btn btn-sm btn-outline-danger btn-destroy" ><i class="fas fa-trash-alt"></i></button>{{--must be after btn-publish--}}
<div class="heading editrubrique imagenode{{ isset($y) && $y == 0 ? ' first' : '' }}" 
  style="background-image: url('{!! asset( $rubrique->background_img_url ) !!}');" data-rubrique_id="{!! $rubrique->id !!}" data-image-src="{!! asset( $rubrique->background_img_url ) !!}">
  {{-- -webkit-image-set: url('{!! asset( $rubrique->background_img_url ) !!}') 1x, url('{!! asset( $rubrique->background_hd_url ) !!}') 2x ;--}}
    {{-- image-set: url('{!! asset( $rubrique->background_img_url ) !!}') 1x, url('{!! asset( $rubrique->background_hd_url ) !!}') 2x ;" --}}
  {!! $rubrique->contenu !!}
</div>

<a class="more" href="#blocs-rubrique{{ $rubrique->id }}">Voir</a>
