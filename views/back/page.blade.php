@extends('themes.themebase.template')

@section('title')
  <title>{{ $page->title }}</title>
  <link rel="stylesheet" href="/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="/css/admin.css">
@endsection

@section('sidebar')
  @include('common.back.inc.sidebar')
@endsection

@section('menu')
  @include('themes.themebase.menu')
@endsection

@section('contenu')

  <p id="id_page" class="d-none">{{ $page->id }}</p>

    @foreach($page->rubriques()->orderBy('place')->get() as $y => $rubrique)

      <div class="main-container">
        <div class="rubrique-container">

          @include('themes.themebase.back.inc.partial_rubrique')

        </div><!--rubrique-->

        <div class="container after-rubrique" data-rubrique_id="{!! $rubrique->id !!}" data-rubrique_cols="{!! $rubrique->cols !!}">

          {{--@include('themes.themebase.back.inc.partial_bloc')--}}
          @include('common.back.inc.partial_bloc')

        </div><!--blocs-->
      </div><!--main-container-->

    @endforeach

    @if($page->slug == 'contact')

      @include('themes.themebase.front.contact')

    @endif
      
@endsection

@if(isset($footer))
  @section('footer')
    @include('themes.themebase.footer')
  @endsection
@endif

@section('scripts')
  <script src="/js/tinymce/tinymce.min.js"></script>
  <script src="/js/tempus-dominus/moment-with-locales.min.js"></script>
  <script src="/js/tempus-dominus/tempusdominus-bootstrap-4.min.js"></script>
  <script src="/js/admin.js"></script>
  <script src="/js/contact.js"></script>
@endsection
